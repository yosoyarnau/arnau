/**
 * 
 */
/**
 *  @author Arnau
 * @date 31/05/2016
 * @version 1.0
 */
import javax.swing.JOptionPane;
public class Pdf19Ej04App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String R=JOptionPane.showInputDialog("Introduce el radio");
		double radio= Double.parseDouble(R);
		
		double area=Math.PI * Math.pow(radio, 2);
		
		double resultado=Math.round(area*100.0)/100.0; //redondear a 2 decimales
		    
		JOptionPane.showMessageDialog(null, "El area del circulo es "+resultado);
		
		
		
		
	}

}
