/**
 * 
 */

/**
 *  @author Arnau
 * @date 31/05/2016
 * @version 1.0
 *
 */
import javax.swing.JOptionPane;
public class Pdf19Ej06App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String Precio=JOptionPane.showInputDialog("Introduce el Precio");
		double P= Double.parseDouble(Precio);
		double IVA=0.21;
		double PF=P*IVA+P;
		
		JOptionPane.showMessageDialog(null, "El precio es "+PF);
	}

}
