/**
 * 
 */

/**
 * @author Arnau
 * @date 27/05/2016
 * @version 1.0
 *
 */
public class Ej05App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A,B,C,D;
		
		A=23;
		B=6;
		C=17;
		D=890;
		
		System.out.println(A+" A, "+B+" B, "+C+" C, "+D+" D");
		
		B=C;
		C=A;
		A=D;
		D=B;
		System.out.println(A+" A, "+B+" B, "+C+" C, "+D+" D");
	}

}
